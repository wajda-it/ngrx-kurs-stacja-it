import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { fromEvent, Observable, Observer, Subscription, EMPTY } from 'rxjs';
import { share } from 'rxjs/operators';
import { ListComponent } from '../components/list/list.component';
import { LoginResponseDto, GetSessionResponseDto, ProfileModel } from "@core";
import { myAjax } from '../observables';

@Component({
  selector: 'rxjs-observable',
  template: `
    <mat-toolbar>Observable</mat-toolbar>
    <form>
      <mat-form-field>
        <mat-label>Email</mat-label>
        <input #email matInput value="piotr@myflow.pl" />
      </mat-form-field>

      <mat-form-field>
        <mat-label>Password</mat-label>
        <input #password matInput type="password" value="123" />
      </mat-form-field>

      <button #btn mat-raised-button color="primary">Button</button>
    </form>
    <pre>{{profile | json}}</pre>
  `
})
export class ObservableComponent implements AfterViewInit {
  @ViewChild('btn', { read: ElementRef })
  btn: ElementRef;
  @ViewChild('email', { read: ElementRef })
  email: ElementRef;
  @ViewChild('password', { read: ElementRef })
  password: ElementRef;
  profile: ProfileModel = null;

  constructor(private list: ListComponent) {}

  ngAfterViewInit() {
    const log = (...args) => this.list.add(...args);
    const button = this.btn.nativeElement;
    const email = this.email.nativeElement;
    const password = this.password.nativeElement;

    // ---------------------------------------------------------------------

    // 1 - Źródło
    const btn$: Observable<MouseEvent> = fromEvent(button, 'click');

    // 2 - Subskrypcja
    // log('SUB')
    // const sub: Subscription = btn$.subscribe(
    //   val => log('next', val),
    //   err => log('error', err),
    //   () => log('complete')
    // );

    // 3 - unsubscribe
    // sub.unsubscribe();

    // ---------------------------------------------------------------------

    /**
     * Tworzenie własnego źródła
     */
    // const btn2$ = myFromEvent(button, 'click');

    // // Subskrybent A
    // log('SUB A');
    // const subA = btn2$.subscribe(
    //   val => log('A next', val),
    //   err => log('A error', err),
    //   () => log('A complete')
    // );
    // setTimeout(() => {
    //   log('UN SUB A');
    //   subA.unsubscribe();
    // }, 2000);

    // // Subskrybent B
    // log('SUB B');
    // const subB = btn2$.subscribe(
    //   val => log('B next', val),
    //   err => log('B error', err),
    //   () => log('B complete')
    // );
    // setTimeout(() => {
    //   log('UN SUB B');
    //   subB.unsubscribe();
    // }, 4000);

    // ---------------------------------------------------------------------

    /**
     * Opakowanie Promise w Observable
     */
    const apiUrl = '/api/quotes?q=the';

    // const ajax$ = myAjax(apiUrl);

    // const sub = ajax$.subscribe(
    //   data => log('SUCCESS', data),
    //   err => log('ERROR', err),
    //   () => log('COMPLETE')
    // );

  }
}
