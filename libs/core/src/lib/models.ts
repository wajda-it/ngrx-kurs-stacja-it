
export interface QuoteModel {
  id: number;
  text: string;
  author: string;
}

export interface GetQuotesResponseDto {
  page: number,
  pageSize: number,
  total: number,
  data: QuoteModel[],
}

export interface UserModel {
  id: number;
  name: string;
  email: string;
  password: string;

}

export interface ProfileModel {
  id: number;
  userId: number;
  description: string;
  photoUrl: string;
}
export interface SessionModel {
  token: string;
  user: UserModel
}

export interface LoginResponseDto {
  token: string;
}

export interface GetSessionResponseDto {
  token: string;
  user: UserModel;
}
