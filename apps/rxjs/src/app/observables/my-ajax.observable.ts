/**
 * Promise based request
 */
const ctrl = new AbortController();

function ajax<T>(url, signal): Promise<T> {
  return <any> fetch(url, { signal: signal })
    .then(res => res.json())
    .then(data => console.log('SUCCESS', data))
    .catch(err => console.log('ERROR', err));
}
// ajax('/api/qotes', ctrl.signal).then()
// ctrl.abort();

/**
 * Observable based Request
 */
export function myAjax(url) {

}
